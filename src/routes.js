import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Product from './pages/Product'
import DetailProduct from './pages/DetailProduct';
import App from './App';
import Buku from './pages/Buku';
import DetailBuku from './pages/DetailBuku';
import Register from './pages/auth/Register';
import Login from './Login';


const RouteApp = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<App/>} />
                    <Route path="/product" element={<Product/>}>
                        <Route path="/product/:productId" element={<DetailProduct/>} />
                    </Route>
                    <Route path="/buku" element={<Buku/>}>
                        <Route path="/buku/:bukuId" element={<DetailBuku/>} />
                    </Route>
                    <Route path="*" element={<h1>404 not found</h1>} />
                    <Route path="/register" element={<Register/>}></Route>
                    <Route path="/login" element={<Login/>}>
                    </Route>
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default RouteApp