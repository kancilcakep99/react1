import { Outlet, useNavigate } from "react-router-dom"

 const Product = () => {
     const navigation = useNavigate()
  return (
      <>
        <h1>Halaman Product</h1>
        <button onClick={() => navigation("/product/12")}>Detail Product</button>
        <Outlet />
      </>
    
  )
}


export default Product