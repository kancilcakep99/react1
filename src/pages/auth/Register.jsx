import  axios  from 'axios';
import {useState, useEffect} from 'react'

 const Register = () => {
    const [image, setImage] = useState();

    const handleImage = (e) => {
        const file = e.target.files[0]
        const reader = new FileReader();
        reader.addEventListener('load', () => {
        setImage(reader.result)
            
        }, false)
        reader.readAsDataURL(file)
        console.log(image)
    }

    const getDataCar = async () => {
        const {data} = await axios("https://rent-cars-api.herokuapp.com/admin/car")
        console.log(data)
    }

    useEffect(() => {
        document.body.style.backgroundColor = "salmon";
        getDataCar();
        return () => {
            document.body.style.backgroundColor = "black";
        }
    }, [image])

    
  return (
      <>
        <h1>Halaman Register</h1>
        { image !== undefined ? (
        <img src={image} alt="Image Preview" style={{width: "100px", height: "100px"}}aria-hidden />
        ): null}
        <input type="file" onChange={(e) => handleImage(e)} />
      </>
    
  )
}


export default Register