import { useParams } from "react-router-dom"

const DetailBuku = () => {
    const {bukuId} = useParams()
    return <h1>Halaman Detail Buku dari Id : {bukuId}</h1>
    
}

export default DetailBuku