import { useParams } from "react-router-dom"

const DetailProduct = () => {
    const {productId} = useParams()
    return <h1>Halaman Detail Product dari Id : {productId}</h1>
    
}

export default DetailProduct