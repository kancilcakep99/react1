import {Component} from 'react';
class InputNama extends Component {
    constructor({label, placeholder}){
        super();
        this.state = {
            number: 0
        }
    }

    // setstate untuk replace angka 1
    // componentDidMount() {
    //     this.setState({angka: 2})
    // }
    render() {
        const {label, type, placeholder} = this.props;
        return (
            <>
            {this.state.number}
            <button onClick={() => this.setState({number: this.state.number + 1})}>
                Tambah
            </button>
            <label>{label}</label>
            <input type={type} id="inputnama" placeholder={placeholder}/>
            </>
        )
    }
}

export default InputNama