import { Component } from 'react';
import style from './Card.module.css';



class Card extends Component {
    // utk ngebuat state perlu panggil constructor di class component
    constructor(){
        super();
        this.state = {
            isWork: false,
        }
    }

    componentDidMount() {
        document.body.style.backgroundColor = "grey"
    }


    componentWillMount() {
        console.log(`Halo...`)
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps)
    }

    componentWillUnmount() {
        document.body.style.backgroundColor = "white"
    }
    // untuk menampilkan html perlu method render
    render() {
        return (
            <>
            <section className={`App ${style.card}`}>
                <h1>"Richard"</h1>
                <button onClick={() => this.setState({ isWork: true})}>Ubah Status Pekerjaan</button>  
            </section>
            </>
        )
    }
}

export default Card