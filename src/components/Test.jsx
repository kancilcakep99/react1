import React, { Component } from 'react'
import axios from "axios"

export default class Test extends Component {
    constructor(){
        // method super utk akses this nya
        super()
        this.state = {
            test: [],
            isUpdate: false,
        }
    }

    async getDataCar(){
        const {data} = await axios("https://rent-cars-api.herokuapp.com/admin/car")
        console.log(data)

        this.setState ({ test: data });
        
    }

    shouldComponentUpdate(newProps, newState) {
        return true
    }

    componentDidUpdate() {
        console.log(this.state);
    }

    componentDidMount(){
        console.log(this.state)
        this.getDataCar();
        setTimeout(() => {
            this.setState({ isUpdate: true})
        }, 2000)
    }
  render() {
    return (
      <>
      {/* dpt melihat data di request data API challenge 4 Rent Car ambil data jenis nama dan category */}
      {this.state.test.map((car) => {
          return (
              <div key={car.id}>
                <h1>{car.name}</h1>
                <p>{car.category}</p>
              </div>
          )
      })}
        
      </>
    )
  }
}
