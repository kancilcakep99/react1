import { useState } from "react";

import axios from 'axios'


const Login = () =>{
    const [username, setUsername] = useState()
    const [password, setPassword] = useState()

    
    const checkLogin = async (e)=>{
        e.preventDefault()
        console.log(e)
        const dataInput = {
            email: username,
            password,
        };
        const data = await axios.post(
            `https://rent-cars-api.herokuapp.com/admin/auth/login`,
            dataInput
        );
        console.log(data)
        
        
    }

    return (
        <form action="">
            <input type="username" value={username} placeholder="masukan username" onChange={(e) => setUsername(e.target.value)} />
            <input type="password" value={password} placeholder="masukan password" onChange={(e) => setPassword(e.target.value)} />
            <button onClick={(e) => checkLogin(e)}>Login</button>
        </form>

    )
}

export default Login