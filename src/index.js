import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import RouteApp from './routes'


ReactDOM.render(
  <React.StrictMode>
    <RouteApp/>
      
    
    {/* <Card nama="Gerry" status="Mahasiswa"/>
    <Card nama="Kak Raihan" status="Mentor"/>
    <Hello>
      <h1>Hello World</h1>
      <p>this description</p>
    </Hello>
    <InputNama />
    <App />
    <HelloAgain />
    <IniComponent />
    <Apps />
    <Submit />
    <Logout /> */}
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

