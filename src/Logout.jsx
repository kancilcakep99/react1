import React, {Component} from "react";

class Logout extends Component {
    handleLogout = e => {alert('Anda logout.')}
    render() {
        return (
            <div className="container">
                <button onClick={this.handleLogout}>Logout</button>
            </div>
        )
    }
}

export default Logout;