import logo from './logo.svg';
import './App.css';
import Card from './components/Card';
import Hello from './Hello';
import InputNama from './InputNama';
import HelloAgain from './HelloAgain';
import IniComponent from './IniComponent';
import Apps from './Apps';
import Submit from './Submit';
import Logout from './Logout';
import Tex from './components/Tex';
import Test from './components/Test';
import {Link, NavLink} from 'react-router-dom'


import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState, useRef } from 'react';
import {useNavigate} from 'react-router-dom'



function App() {
  const [isShow,setIsShow] = useState(true)
  const [nama] = useState("Gerry")
  // const [countRender, setCountRender] = useState(0)
  const navigate = useNavigate()
  const countRender = useRef(0)
  const inputRef = useRef(0)
  console.log(countRender)

  useEffect(() => {
    countRender.current = countRender.current + 1
    inputRef.current.focus()
    // setCountRender((prevState) => prevState + 1)
  }, [])

  return (
    <> 
    <div className="App">
      <header className="App-header">
        
        {countRender.current}
        <input type="number" placeholder="masukan angka" ref={inputRef} />
        <button onClick={() => navigate('/pindah')}>click</button>
      <Link to="/product">Product</Link>
    <NavLink to="/" style={({isActive}) => isActive ? {color: "salmon"} : {color: "black"}}>Home</NavLink>
      <Link to="/buku">Buku</Link>
      <Link to="/register">Login</Link>
      <Link to="/login">Login</Link>
        <img src={logo} className="App-logo" alt="logo" />
        {/* memanggil degn single stack */}
        <Test/>
        <Tex/>
        {isShow ? <Card /> : null}
        <p>{nama}</p>
        <button onClick={() => setIsShow(false)}>Hide</button>
        
        <Hello>
          <h1>Hello World</h1>
          <p>this description</p>
        </Hello>
        <InputNama label="Input Nama" type="text" placeholder="Masukan Nama">
          <h1>Input Nama</h1>
        </InputNama>
        <HelloAgain/>
        <IniComponent/>
        <Apps/>
        <Submit/>
        <Logout/>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
    </>
  );
}

export default App;
